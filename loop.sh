#!/usr/bin/env bash
set -x -e

cd ..
cd timing-

for _ in {0..1024}
do
   p="$((RANDOM%64))/$((RANDOM%64))/d.txt"
   shuf -o "$p" "$p"
   time pijul record -am"."
done

echo "OK"
