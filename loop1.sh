#!/usr/bin/env bash
set -x -e

cd ..
cd timing-

for _ in {0..1024}
do
   p="1/1/d.txt"
   shuf -o "$p" "$p"
   time pijul record -am"."
done

echo "OK"
