#!/usr/bin/env bash
set -x -e

cd ..
rm -rf timing-

pijul init timing-
cd timing-

for i in {0..64}
   do
      for j in {0..64}
         do
            mkdir -p "$j/$i"
            cp ../pijul-timing/d.txt "$j/$i/"
   done
done
pijul add -r .
pijul record -am "."
echo "OK"
